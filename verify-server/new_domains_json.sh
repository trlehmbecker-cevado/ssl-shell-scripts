#!/bin/bash

# set -o errexit
# set -o pipefail
set -o nounset

report_command_status() {
	if [[ $? -eq 0  ]]; then
                echo "DONE"
        else
                echo "FAILED"
        fi
}

create_domain_dir() {
        if [[ -d "${WEB_ROOT}/${DOMAIN}" ]]; then
                echo "${WEB_ROOT}/${DOMAIN} already exists"
                return 0
        fi
        echo -n "Creating web directory..."
	mkdir "${WEB_ROOT}/${DOMAIN}"
        report_command_status
	return $?
}

create_challenge_dir() {
        CHALLENGE_DIR="${WEB_ROOT}/${DOMAIN}/${WELL_KNOWN}/${ACME_CHALLENGE}"
        if [[ -d "${CHALLENGE_DIR}" ]]; then
                echo "${CHALLENGE_DIR} already exists"
		return 0
	else
		echo -n "Creating challenge directory..."
		mkdir -p "${CHALLENGE_DIR}"
		report_command_status
		return $?
        fi
}

create_challenge() {
	echo -n "Creating ACME Challenge..."
	echo "${FILE_CONTENTS}">"$CHALLENGE_DIR/${FILE_NAME}"
	report_command_status
	return $?
}

while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-f|--file)
		out_file="$2"
		shift
		shift
		;;
		--overwrite)
		overwrite=1
		shift
		;;
		*)
		shift
		;;
	esac
done

if [[ $# -lt 1  ]]; then
	echo "Missing JSON file"
	exit 1
fi

JSON=$1
WEB_ROOT="/var/www"
WELL_KNOWN=".well-known"
ACME_CHALLENGE="acme-challenge"
NUM_NEW_DOMAINS=$(jq -r '. | length' ${JSON})
echo "Found ${NUM_NEW_DOMAINS} new domains"

# Read objects in JSON array (-rc = raw, comppact)
jq -rc .[] ${JSON} | while IFS='' read domain; do
	DOMAIN=$(echo "$domain" | jq -r .domain)
	FILE_NAME=$(echo "$domain" | jq -r .file_name)
	FILE_CONTENTS=$(echo "$domain" | jq -r .file_contents)
	
	echo "------ ${DOMAIN} ------"

	create_domain_dir
	if [[ $? -eq 1  ]]; then
		exit 1
	fi

	create_challenge_dir
	if [[ $? -eq 1  ]]; then
                exit 1
        fi

	create_challenge
	if [[ $? -eq 1  ]]; then
                exit 1
        fi	

	/bin/bash /home/ec2-user/new_host.sh "${DOMAIN}"
	if [[ $? -eq 1  ]]; then
                exit 1
        fi
done
