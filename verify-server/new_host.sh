#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

# Argument checking and assignment,
# Usage: new_domains.sh -i input.json
while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-d|--domain)
        if [ -z ${2+x} ]; then
            echo "Missing domain"
            exit 1
        fi
		domain="$2"
		shift
		shift
		;;
		*)
		shift
		;;
	esac
done

if [ -z ${domain+x} ]; then
    echo "Missing required -d argument"
    exit 1
fi
# TODO: CHANGE TO /etc/httpd/conf.d for production/staging
apache_host_root="/home/ec2-user"
hosts_file="hosts.conf"
macro="vhost"

host_entry="Use ${macro} ${domain}"

# Search the hosts file for existing vhost (-q=quiet, -s=suppress)
if grep -qs "${host_entry}" ${apache_host_root}/${hosts_file}; then
    echo "Host already exists"
    exit 0
else
    echo -n "Adding new host entry..."
    echo "${host_entry}" >> ${apache_host_root}/${hosts_file}
    if [[ $? -eq 0 ]]; then
		echo "DONE"
		exit 0
	else
		echo "FAILED"
		exit 1
	fi
fi
