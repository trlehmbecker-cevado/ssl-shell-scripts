#!/bin/bash

#set -o errexit
#set -o pipefail
set -o nounset

# Argument checking and assignment,
# Usage: fetch_domains.sh -f outfile.json --overwrite
while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-f|--file)
		if [ -z ${2+x} ]; then
            echo "Missing output file name"
            exit 1
        fi
		out_file="$2"
		shift
		shift
		;;
		--overwrite)
		overwrite=1
		shift
		;;
		*)
		shift
		;;
	esac
done

# Check for out file argument
if [ -z ${out_file+x} ]; then
	echo "Missing required -f argument"
	exit 1
fi

#TODO: CHANGE TO ACTUAL API ENDPOINT FOR PRODUCTION/STAGING
# API endpoint to retreive domains waiting for verification
endpoint="https://tristanlehmbecker.com/sample/test.json"

# Stores endpoint response (-qO- = quiet, output to current stream)
echo -n "Fetching new domains..."
response=$(wget -qO- "${endpoint}")
if [[ $? -ne 0 ]]; then
	echo "FAILED"
	exit 1
elif [[ -z "${response}"  ]]; then
	echo "FAILED - Empty response"
	exit 1
fi
echo "DONE"

# Make sure that JSON isn't empty array
if [[ "${response}" != "[]" ]]; then
	response=$(echo "${response}" |  jq '.results')
fi

# Write/Append to array
echo -n "Checking ${out_file} exists: "
write_verb="Writing"
if [[ -f "${out_file}" ]]; then
	# ${overwrite:-0} = if $overwrite isn't set (or empty?), substitute 0
	if [[ ${overwrite:-0} -eq 1 ]]; then
		echo "Yes, overwrite is set"
	else
		echo "Yes"
		write_verb="Appending"
	fi
else
	echo "No"
fi

echo -n "${write_verb} to ${out_file}..."
if [[ ${write_verb} == "Writing" ]]; then
	echo "${response}" > "${out_file}"
else
	# Load existing file into [0]
	# Concantenate the response from API endpoint
	# Write back to file
	joined_json=$(jq -s ".[0] + ${response}" ${out_file})
	echo "${joined_json}" > "${out_file}"
fi

if [[ $? -eq 0 ]]; then
	echo "DONE"
	exit 0
else
	echo "FAILED"
	exit 1
fi
