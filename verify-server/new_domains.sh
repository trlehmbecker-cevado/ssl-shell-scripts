#!/bin/bash

#set -o errexit
#set -o pipefail
set -o nounset

report_command_status() {
	if [[ $? -eq 0  ]]; then
        echo "DONE"
    else
        echo "FAILED"
    fi
}

create_domain_dir() {
    if [[ -d "${web_root}/${domain}" ]]; then
            echo "${web_root}/${domain} already exists"
            return 0
    fi
    echo -n "Creating web directory..."
	mkdir "${web_root}/${domain}"
    report_command_status
	return $?
}

create_challenge_dir() {
    challenge_dir="${web_root}/${domain}/${well_known}/${acme_challenge}"
    if [[ -d "${challenge_dir}" ]]; then
        echo "${challenge_dir} already exists"
        return 0
	else
		echo -n "Creating challenge directory..."
		mkdir -p "${challenge_dir}"
		report_command_status
		return $?
    fi
}

create_challenge() {
	echo -n "Creating ACME Challenge..."
	echo "${file_contents}">"$challenge_dir/${file_name}"
	report_command_status
	return $?
}

# Argument checking and assignment,
# Usage: new_domains.sh -i input.json
while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-i|--input)
        if [ -z ${2+x} ]; then
            echo "Missing input file"
            exit 1
        fi
		in_file="$2"
		shift
		shift
		;;
		*)
		shift
		;;
	esac
done

# Check for input argument
if [ -z ${in_file+x} ]; then
    echo "Missing required -i argument"
    exit 1
fi

web_root="/var/www"
well_known=".well-known"
acme_challenge="acme-challenge"
num_new_domains=$(jq -r '. | length' ${in_file})

echo "Found ${num_new_domains} new domains"

# Read objects in in_file JSON array (-rc = raw, comppact)
jq -rc '.[]' ${in_file} | while IFS='' read object; do
	domain=$(echo "${object}" | jq -r '.domain')
	file_name=$(echo "${object}" | jq -r '.file_name')
	file_contents=$(echo "${object}" | jq -r '.file_contents')
	
	echo "------ ${domain} ------"

	create_domain_dir
	if [[ $? -eq 1  ]]; then
		exit 1
	fi

	create_challenge_dir
	if [[ $? -eq 1  ]]; then
        exit 1
    fi

	create_challenge
	if [[ $? -eq 1  ]]; then
        exit 1
    fi	

	#TODO: CHANGE ONCE SCRIPT IS AVAILABLE IN PATH OR MOVED TO PRODUCTION DIR
	/bin/bash /home/ec2-user/new_host.sh -d "${domain}"
	if [[ $? -eq 1  ]]; then
        exit 1
    fi
done
