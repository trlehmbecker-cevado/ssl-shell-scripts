import sys
import os
import utilities
import argparse

# Argument handling
parser = argparse.ArgumentParser()
parser.add_argument('domain', type=str)
parser.add_argument('proxy', nargs='?', default='')
parser.add_argument('--http', action='store_true')
args = parser.parse_args()

if not args.http and not args.proxy:
    parser.error("Proxy is required!")
    sys.exit(1)

APACHE_HOST_ROOT = '/home/ec2-user'
HOST_FILE = 'hosts.conf'
if args.http:
    MACRO = 'VHost80'
else:
    MACRO = 'VHost'

host_file = f'{APACHE_HOST_ROOT}/{HOST_FILE}'
host_entry = f'Use {MACRO} {args.domain}'

if not args.proxy:
    host_entry += '\n'
else:
    host_entry += f' {args.proxy}\n'
    
write_mode = 'w'

print(host_entry)
if os.path.exists(host_file):
    write_mode = 'a'
    try:
        if utilities.file_has_target(host_file, host_entry) == True:
            print("Host already exists")
            sys.exit(0)
    except Exception as e:
        print("EXCEPTION")
        print(e)
        sys.exit(1)
        
try:
    with open(host_file, write_mode) as host_file:
        host_file.write(host_entry)
    print("DONE")
except Exception as e:
    print("EXCEPTION")
    print(e)
    sys.exit(1)