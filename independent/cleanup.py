import sys
import os
import argparse
import shutil
import utilities

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--domains', nargs='*')
group.add_argument('--all', action='store_true')
args = parser.parse_args()

APACHE_HOST_ROOT = '/home/ec2-user'
HOST_FILE = 'hosts.conf'
WEB_ROOT = '/var/www'
IGNORED_DIRS = ['html', 'cgi-bin']

dirs = []

if args.all:
    domain_dirs = os.listdir(WEB_ROOT)
    for domain_dir in domain_dirs:
        if domain_dir not in IGNORED_DIRS:
            dirs.append(domain_dir)
elif len(args.domains) > 0:
    for domain in args.domains:
        dirs.append(domain)
else:
    print("No domains specificied")
    sys.exit(0)
    
print(f"Found {len(dirs)} domains to delete")
host_file = f'{APACHE_HOST_ROOT}/{HOST_FILE}'
for dir in dirs:
    print('------ {} ------'.format(dir))
    # Remove VHost references
    print("Removing VHost(s)...", end='')
    escaped_domain = dir.replace('.', '\.')
    host_regex = f'^Use (VHost80|VHost) {escaped_domain}.*$'
    num_vhosts_removed = utilities.remove_lines_regex(host_file, host_regex)
    print(f"DONE - {num_vhosts_removed} VHosts removed")
    
    # Remove web directories
    abs_dir = f'{WEB_ROOT}/{dir}'
    if os.path.exists(abs_dir): 
        print(f"Deleting {abs_dir}...", end='')
        try:
            shutil.rmtree(abs_dir)
            print("DONE")
        except Exception as e:
            print("EXCEPTION")
            print(e)
            sys.exit(1)
    else:
        print("No web directory")