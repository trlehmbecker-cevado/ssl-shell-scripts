import sys
import os, errno
import utilities
import json
import argparse
from json.decoder import JSONDecodeError

# Arguemtn handling
parser = argparse.ArgumentParser()
parser.add_argument('in_file', type=str)
args = parser.parse_args()

WEB_ROOT='/var/www'
WELL_KNOWN='.well-known'
CHALLENGE='acme-challenge'

try:
    in_json = utilities.read_json(args.in_file)
except FileNotFoundError:
    print("in_file not found")
    sys.exit(1)
except JSONDecodeError:
    print("in_file has invalid JSON data")
    sys.exit(1)
    
print("Found {} domains".format(len(in_json)))

# Any exception other than EEXIST caused by os.makdirs is thrown
def create_dir(directory):
    # Try catch here to prevent possible race condition
    try:
        os.makedirs(directory)
        print("DONE")
    except OSError as e:
        if e.errno == errno.EEXIST:
            print("DONE - ALREADY EXISTS")
        else:
            print("EXCEPTION")
            print(e)
            sys.exit(1)

def create_challenge(out_path, content):
    try:
       with open(out_path, 'w') as out_file:
           out_file.write(content) 
       print("DONE")
    except Exception as e:
        print("EXCEPTION")
        print(e)
        sys.exit(1) 

for domain in in_json:
    print('-------- {} ---------'.format(domain['domain']))
    web_dir = '{}/{}'.format(WEB_ROOT, domain['domain'])
    print("Creating web directory...", end='')
    create_dir(web_dir)

    challenge_dir = '{}/{}/{}'.format(web_dir, WELL_KNOWN, CHALLENGE)
    print("Creating challenge directory...", end='')
    create_dir(challenge_dir)

    challenge_file = '{}/{}'.format(challenge_dir, domain['file_name'])
    print("Creating ACME Challenge...", end='')
    create_challenge(challenge_file, domain['file_contents'])
    
    
    
    
    
    