import sys
import os
import utilities
import requests
import json
import argparse

# Argument handling
parser = argparse.ArgumentParser()
parser.add_argument('out-file', type=str)
parser.add_argument('--overwrite', action='store_true')
args = parser.parse_args()

# TODO: CHANGE TO ACTUAL API ENDPOINT
url = 'https://tristanlehmbecker.com/sample/test.json'
payload = {}

print("Fetching new domains...", end='')
try:
    r = requests.get(url, params=payload)
    print("DONE")
    r.raise_for_status()
except requests.exceptions.HTTPError as err:
    print(err)
    sys.exit(1)
except requests.exceptions.Timeout:
    print("Connection timed out")
    sys.exit(1)
except requests.exceptions.ConnectionError:
    print("Connection error")
    sys.exit(1)
except requests.exceptions.RequestException:
    print("Other request error")
    sys.exit(1)
    
response = r.json()

if 'results' in response:
    num_new_domains = len(response['results'])
    out_file_exists = os.path.isfile(args.out_file)
    
    if num_new_domains > 0:
        print("Got {} new domains".format(num_new_domains))
        print("Checking {} exists: {}".format(args.out_file, out_file_exists))
        out_json = []
        existing_domains = []
        
        if out_file_exists and not args.overwrite:
            out_json = utilities.read_json(args.out_file)
            
            for a in out_json:
                existing_domains.append(a['domain'])
                
        for obj in response['results']:
            if obj['domain'] not in existing_domains:
                out_json.append(obj)
        
        with open(args.out_file, 'w') as out_file:
            out_file.write(json.dumps(out_json))
    else:
        print("No new domains")
else:
    print("No new domains")
