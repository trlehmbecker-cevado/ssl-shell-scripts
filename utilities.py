import sys
import os, errno
import json
import re
import fileinput
import requests

def read_json(path):
    with open(path, 'r') as input_file:
        return json.load(input_file)
    
def file_has_target(path, target):
    with open(path, 'rt') as input_file:
        for line in input_file:
            if target in line:
                return True
    return False
    
def remove_lines_regex(path, regex):
    lines_removed = 0
    with fileinput.input(path, inplace=True, mode='r') as input:
        for line in input:
            if not re.search(regex, line):
                print(line, end='')
            else:
                lines_removed += 1
    return lines_removed

def remove_file_contents(path):
    open(path, 'w').close()

def create_dir(directory):
    try:
        os.makedirs(directory)
        return True
    except OSError as e:
        if e.errno == errno.EEXIST:
            return False
        else:
            raise e

def get_instance_id():
    r = requests.get('http://169.254.169.254/latest/meta-data/instance-id')
    return r.text