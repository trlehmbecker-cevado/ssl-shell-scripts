import boto3
import json
import sys
import time

QUEUE = 'https://sqs.us-west-2.amazonaws.com/849248624138/NewDomainQueue.fifo'

def read_queue(sqs_client, n):
    print(f"--- {n} attempt(s)")
    response = sqs_client.receive_message(
        QueueUrl=QUEUE,
        AttributeNames=[
            'SentTimestamp',
            'SequenceNumber'
        ],
        MessageAttributeNames=[
            'server-instance'
        ],
        MaxNumberOfMessages=1,
        WaitTimeSeconds=20
    )
    return response

def listen(instance_id):
    sqs_client = boto3.client('sqs', region_name='us-west-2')
    got_message = False
    n = 0
    while got_message == False:
        n += 1
        response = read_queue(sqs_client, n) # do
        while 'Messages' not in response: # while
            if n > 5:
                print("--- Too many empty responses")
                sys.exit(1)
            print("--- Empty response, retrying in 5s")
            time.sleep(5)
            n += 1
            response = read_queue(sqs_client, n)
            
        messages = response['Messages'][0]
        receipt_handle = messages['ReceiptHandle']
        
        try:
            server_instance = messages['MessageAttributes']['server-instance']['StringValue']
        except KeyError:
            print("--- server-instance isn't specified, can't process")
            sys.exit(1)
        if server_instance != instance_id:
            print(f"--- Intended for: {server_instance}, mine: {instance_id}")
            sqs_client.change_message_visibility(
                QueueUrl=QUEUE,
                ReceiptHandle=receipt_handle,
                VisibilityTimeout=0
            )
            print("--- Sleeping for 60s")
            time.sleep(60)
        else:
            got_message = True
    return receipt_handle

def delete_message(receipt_handle):
    sqs_client = boto3.client('sqs', region_name='us-west-2')
    print("+ Removing Message", end='')
    sqs_client.delete_message(
        QueueUrl=QUEUE,
        ReceiptHandle=receipt_handle
    )
