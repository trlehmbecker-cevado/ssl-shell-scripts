import utilities
import boto3
import requests
import json
import sys
import os
import shutil

WELL_KNOWN='.well-known'
CHALLENGE='acme-challenge'
BUCKET = 'cevado-ssl'

def fetch_domains_s3(out_file, key, overwrite=False):
    s3 = boto3.client('s3', region_name='us-west-2')
    response = s3.get_object(
        Bucket=BUCKET,
        Key=key
    )
    json_response = json.loads(response['Body'].read())
    out_json = []
    if 'results' in json_response:
        num_new_domains = len(json_response['results'])
        out_file_exists = os.path.isfile(out_file)
        write_verb = ''
        if out_file_exists:
            if overwrite:
                write_verb = ', overwrite mode'
            else:
                write_verb = ', append mode'

        if num_new_domains > 0:
            print(f"--- Got {num_new_domains} new domains")
            print(f"--- Checking {out_file} exists: {out_file_exists}{write_verb}")

            existing_domains = []
            
            if out_file_exists and not overwrite:
                out_json = utilities.read_json(out_file)
                for obj in out_json:
                    existing_domains.append(obj['domain'])
                    
            for obj in json_response['results']:
                if obj['domain'] not in existing_domains:
                    out_json.append(obj)
            
            with open(out_file, 'w') as out_file:
                out_file.write(json.dumps(out_json))
    return out_json

def fetch_domains_url(url, out_file, overwrite=False):
    try:
        r = requests.get(url)
        r.raise_for_status()
    except requests.exceptions.HTTPError as err:
        print(err)
        sys.exit(1)
    except requests.exceptions.Timeout:
        print("Connection timed out")
        sys.exit(1)
    except requests.exceptions.ConnectionError:
        print("Connection error")
        sys.exit(1)
    except requests.exceptions.RequestException:
        print("Other request error")
        sys.exit(1)
        
    response = r.json()
    out_json = []
    if 'results' in response:
        num_new_domains = len(response['results'])
        out_file_exists = os.path.isfile(out_file)
        
        if num_new_domains > 0:
            print(f"--- Got {num_new_domains} new domains")
            print(f"--- Checking {out_file} exists: {out_file_exists}")
            
            existing_domains = []
            
            if out_file_exists and not overwrite:
                out_json = utilities.read_json(out_file)
                
                for a in out_json:
                    existing_domains.append(a['domain'])
                    
            for obj in response['results']:
                if obj['domain'] not in existing_domains:
                    out_json.append(obj)
            
            with open(out_file, 'w') as out_file:
                out_file.write(json.dumps(out_json))
    return out_json


def create_domain_resources(root, domain):
    web_dir = f'{root}/{domain["domain"]}'
    print("--- Creating web directory")
    utilities.create_dir(web_dir)

    challenge_dir = f'{web_dir}/{WELL_KNOWN}/{CHALLENGE}'
    print("--- Creating challenge directory")
    utilities.create_dir(challenge_dir)

    challenge_file = f'{challenge_dir}/{domain["file_name"]}'
    print("--- Creating ACME Challenge")
    with open(challenge_file, 'w') as out_file:
        out_file.write(domain['file_contents'])

def create_virtualhost(apache_host, domain):
    type_macro = {
        1: 'VHost',
        2: 'VHost80',
        3: 'Verify'
    }
    try:
        macro = type_macro[domain['type']]
    except KeyError as key:
        #TODO Airbrake non-critical error
        #TODO Add domain to list of domains not added for report
        if key == 'type': pass #TODO reason: missing type
        else: pass #TODO reason: bad type
        print("MISSING TYPE")
        return False

    proxy = ''
    if macro != 'Verify':
        try:
            proxy = domain['proxy']
        except KeyError:
            #TODO Airbrake non-critical error
            #TODO Domain report: proxy required for HTTP/HTTPS host
            print("MISSING PROXY")
            return False

    print(f"--- Creating {macro} virtualhost")
    try:
        return make_virtualhost(
            apache_host=apache_host,
            domain_name=domain['domain'],
            macro=macro,
            proxy=proxy
        )
    except KeyError as ke:
        #TODO Airbrake non-critical error
        #TODO Add domain to list of domains not added for report
        return False
    except OSError as oe:
        pass
        # Airbrake critical error
        # Report OS Error

def make_virtualhost(*, apache_host, domain_name, macro, proxy=''):
    host_entry = f'Use {macro} {domain_name}'
    if not proxy:
        host_entry += '\n'
    else:
        host_entry += f' {proxy}\n'

    write_mode = 'w'
    if os.path.exists(apache_host):
        write_mode = 'a'
        #TODO Check for conflicts like HTTP & Verify
        if utilities.file_has_target(apache_host, host_entry):
            return True
    with open(apache_host, write_mode) as host_file:
        host_file.write(host_entry)
    return True

def delete_web_dirs(*, web_root, all=False, domains=[], ignored_dirs=[]):
    dirs_to_remove = []
    if all:
        domain_dirs = os.listdir(web_root)
        for domain_dir in domain_dirs:
            if domain_dir not in ignored_dirs:
                dirs_to_remove.append(domain_dir)
    elif len(domains) > 0:
        dirs_to_remove = list(set(domains).difference(ignored_dirs))
    else: return

    for domain_dir in dirs_to_remove:
        abs_dir = f'{web_root}/{domain_dir}'
        if os.path.exists(abs_dir):
            print(f'--- Deleting {abs_dir}')
            shutil.rmtree(abs_dir)

def remove_virtualhosts(*, host_path, all=False, hosts=[], verify_only=False):
    macro_regex = "(VHost80|VHost|Verify)"
    if verify_only:
        macro_regex = "(Verify)"
    if all:
        print(f"--- Removing all virtualhosts")
        utilities.remove_file_contents(host_path)
    else:
        for host in hosts:
            print(f"--- Removing virtualhost(s) for {host}")
            escaped_domain = host.replace('.', '\.')
            host_regex = f'^Use {macro_regex} {escaped_domain}.*$'
            hosts_removed = utilities.remove_lines_regex(host_path, host_regex)
            print(f" - {hosts_removed} removed")