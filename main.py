import queue_manager
import domain_manager
import tls_manager
import argparse
import subprocess
import utilities

parser = argparse.ArgumentParser()
parser.add_argument('--fetch', action='store_true')
parser.add_argument('--skip-queue', action='store_true')
parser.add_argument('--restart', action='store_true')
# could make default 'all', but potentially dangerous
parser.add_argument('--del-web-dir', nargs='+') 
parser.add_argument('--rm-virtualhosts', nargs='+')
parser.add_argument('--del-tls', nargs='+')
args = parser.parse_args()

WEB_ROOT = '/var/www'
APACHE_HOST = '/etc/httpd/conf.d/hosts.conf'
S3_KEY_MIXED = 'all/mixed.json' 
S3_KEY_HTTPS = 'new/https-only.json'
S3_KEY_HTTP = 'verify/verify-only.json'
OUT_FILE = 'out.json'
INSTANCE_ID = utilities.get_instance_id()

def build_domains(domains):
    for domain in domains:
        print(f"+ {domain['domain']}")
        try:
            domain_type = domain['type']
            if not domain_type:
                raise ValueError("type cannot be empty")
        except ValueError as ve:
            #TODO Airbrake non-critical error
            #TODO Add domain to list of domains not added for report
            print("type cannot be empty")
            continue
        except KeyError as ke:
            #TODO Airbrake non-critical error
            #TODO Add domain to list of domains not added for report
            print("Domain missing type")
            continue

        domain_manager.create_virtualhost(APACHE_HOST, domain)
        if domain_type == 1:
            tls_manager.create_tls_resources(domain)
        if domain_type == 3:
            domain_manager.create_domain_resources(WEB_ROOT, domain)

def delete_web_directories(all, domains=[]):
    if all:
        print(f"+ Deleting ALL web directories ({WEB_ROOT})")
    else:
        print(f"+ Deleting {len(domains)} web directories ({WEB_ROOT})")
    ignored=['html', 'cgi-bin']
    domain_manager.delete_web_dirs(
        web_root=WEB_ROOT,
        all=all,
        domains=domains,
        ignored_dirs=ignored
    )

def remove_virtualhosts(all, hosts=[]):
    if all:
        print(f"+ Deleting ALL virtualhosts ({APACHE_HOST})")
    else:
        print(f"+ Deleting {len(args.rm_virtualhosts)} virtualhosts ({APACHE_HOST})")
    domain_manager.remove_virtualhosts(
        host_path=APACHE_HOST,
        hosts=hosts,
        all=all
    )

def delete_tls(domains):
    for domain in domains:
        tls_manager.delete_tls_resources(domain)

if args.fetch:
    if not args.skip_queue:
        print("+ Listening for messge in queue")
        receipt_handle = queue_manager.listen(INSTANCE_ID)
        print("+ Fetching new domains")
        domains = domain_manager.fetch_domains_s3(OUT_FILE, S3_KEY_MIXED)
        print(f"+ Building {len(domains)} domain(s)")
        build_domains(domains)
        print("+ Deleting message from queue")
        queue_manager.delete_message(receipt_handle)
    else:
        print("+ Fetching new domains")
        domains = domain_manager.fetch_domains_s3(OUT_FILE, S3_KEY_HTTPS)
        print(f"+ Building {len(domains)} domain(s)")
        build_domains(domains)

    if args.restart:
        print('+ Checking Apache')
        try:
            subprocess.run(
                ['./restart_apache.sh', '--test-only'],
                check=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
        except subprocess.CalledProcessError as cpe:
            print("BAD APACHE CONFIG")
            print(f'''
                Code: {cpe.returncode},
                CMD: {cpe.cmd},
                output: {cpe.output},
                stderr: {cpe.stderr}
            ''')
            # Non-zero exit, meaning config is bad
            #TODO Airbrake critical error
            #TODO Report bad config
        print('+ Restarting Apache')
        try:
            subprocess.run(
                ['./restart_apache.sh', '--graceful'],
                check=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
        except subprocess.CalledProcessError as cpe:
            print("FAILED TO RESTART APACHE")
            print(f'''
            Code: {cpe.returncode},
            CMD: {cpe.cmd},
            output: {cpe.output},
            stderr: {cpe.stderr}
            ''')
            # Non-zero exit, meaning something went wrong during restart
            #TODO Airbrake critical error
            #TODO Report apache error
else:
    if args.del_web_dir:
        ignored = ['html', 'cgi-bin']
        if 'all' in args.del_web_dirs:
            delete_web_directories(True)
        else:
            delete_web_directories(False, args.del_web_dirs)    

    if args.rm_virtualhosts:
        if 'all' in args.rm_virtualhosts:
            remove_virtualhosts(True)
        else:
            remove_virtualhosts(False, args.rm_virtualhosts)

    if args.del_tls:
        delete_tls(args.del_tls)