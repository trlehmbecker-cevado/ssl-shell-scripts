import os
import shutil
import utilities

CERT_DIR = '/etc/pki/tls/certs'
PRIV_DIR = '/etc/pki/tls/private'

def create_tls_resources(domain):
    cert_file = f'{CERT_DIR}/{domain["domain"]}'
    key_file = f'{PRIV_DIR}/{domain["domain"]}'

    print("--- Creating Cert file")
    with open(cert_file, 'w') as out_file:
        out_file.write(domain['cert'])
    
    print("--- Creating Key file")
    with open(key_file, 'w') as out_file:
        out_file.write(domain['key'])


def delete_tls_resources(domain):
    cert_file = f'{CERT_DIR}/{domain["domain"]}'
    key_file = f'{PRIV_DIR}/{domain["domain"]}'

    print("--- Deleting Cert file")
    os.remove(cert_file)

    print("--- Deleting Key file")
    os.remove(key_file)
