#!/user/bin/env python3
import boto3
import time
import sys
import utilities

DOMAIN = 'Cvdo-ssl'
INSTANCE_ID = utilities.get_instance_id()
TASKLIST = 'tasklist'
client = boto3.client('swf')
while True:

    response = client.poll_for_activity_task(
        domain=DOMAIN,
        taskList={
            'name': TASKLIST
        },
        identity=INSTANCE_ID
    )

    if 'taskToken' in response:
        token = response['taskToken']
        input = response['activityType']['input']
        try:
            server_instance = input['server-instance']
        except KeyError as ke:
            #TODO Airbrake
            #TODO Activity Failed, no server-instance
            sys.exit(1)

        if server_instance == INSTANCE_ID or server_instance == 'agnostic':
            activity = response['activityType']['name']

            if activity == 'fetchDomains':
                pass
            elif activity == 'buildDomains':
                pass
            elif activity == 'restartApache':
                pass
        else:
            # give other servers a chance to pickup the command
            print("Sleeping for 70s")
            time.sleep(70)
    else:
        print("No task received")