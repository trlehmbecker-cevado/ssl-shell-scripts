#!/bin/bash

while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		--graceful)
		graceful=1
		shift
		;;
        --test-only)
        test_only=1
        shift
        ;;
	esac
done

if [[ ${test_only:-0} -eq 1 ]]; then
    service httpd configtest
else
    if [[ ${graceful:-0} -eq 1 ]]; then
        service httpd graceful
    else
        service httpd restart
    fi
fi
exit $?